# OmdbApp (MVP design pattern, at least I hope so) (screenshots are at the bottom)

With this application you can search for movies, and see their details. The searched movies are cached and can be saved for offline use. 
I used Android-Job for managing the movies: 6 hours after the user hasn't been using the application a Job runs that deletes the oldest movies that hasn't been saved (it can be done in the movies' DetailsFragment). The Job will only delete movies if there are more than 100 unsaved ones in the database.

The app consists of three main parts: MainActivity that hosts the fragments,  MainFragment that lists the movies and DetailsFragment that displays the details of the chosen movie.

MainFragment:
When the the EditText is hidden or empty, the RecyclerView shows the cached/downloaded movies. The toolbar's search button makes a ConstraintLayout visible that consists of a back Button and an EditText. 
At each character entered in the EditText a query runs immediately and updates the RecyclerView's content (it is handled by Realm and RealmRecyclerViewAdapter) and after one second since the last key press a request is sent to OMDB's server and the received list of movies are added to the database. 
The API sends back 10 movies at a time so a footer is visible at the bottom of the RecyclerView and it sends request for the next 10 movies(if the user enters a title that has been searched for before and presses the footer, the app automatically cycles through the downloaded movies and downloads the next 10 movies that hasn't been downloaded yet). 

Used libraries
- Dagger2
- Retrofit2
- Realm
- Glide
- Palette
- Android-Job

Huge thank you to:
- http://omdbapi.com/
- the entire androiddev subreddit

![](screenshots/1.png)
![](screenshots/2.png)
![](screenshots/3.png)
![](screenshots/4.png)
![](screenshots/5.png)