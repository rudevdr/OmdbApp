package com.tiborfarago.omdbapp

import android.app.Application
import com.evernote.android.job.JobManager
import com.github.piasy.biv.BigImageViewer
import com.github.piasy.biv.loader.glide.GlideImageLoader
import com.tiborfarago.omdbapp.androidjob.JobCreator
import com.tiborfarago.omdbapp.di.*
import io.realm.Realm

class App : Application() {
    private lateinit var applicationComponent: ApplicationComponent

    lateinit var mainComponent: MainComponent
        private set

    companion object {
        lateinit var INSTANCE: App
            private set
    }

    override fun onCreate() {
        super.onCreate()
        INSTANCE = this

        Realm.init(this)

        BigImageViewer.initialize(GlideImageLoader.with(this))

        applicationComponent = DaggerApplicationComponent.builder()
                .retrofitModule(RetrofitModule("http://www.omdbapi.com"))
                .build()

        mainComponent = DaggerMainComponent.builder()
                .applicationComponent(applicationComponent)
                .build()

        JobManager.create(this).addJobCreator(JobCreator())
    }
}