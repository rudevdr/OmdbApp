package com.tiborfarago.omdbapp.details


import android.graphics.drawable.TransitionDrawable
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.FrameLayout
import com.github.piasy.biv.view.BigImageView

import com.tiborfarago.omdbapp.R

class PosterFragment : Fragment() {

    private var url: String? = null

    private lateinit var imageView: BigImageView
    private lateinit var frameLayout: FrameLayout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (arguments != null) {
            url = arguments!!.getString(URL)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_poster, container, false)

        imageView = view.findViewById(R.id.poster_fragment_image_view)
        frameLayout = view.findViewById(R.id.fragment_poster_frame_layout)

        imageView.showImage(Uri.parse(url))

        return view
    }

    override fun onCreateAnimation(transit: Int, enter: Boolean, nextAnim: Int): Animation {
        val animator: Animation?
        if (enter && nextAnim == R.anim.poster_enter_fragment_anim) {
            animator = AnimationUtils.loadAnimation(activity, nextAnim)
            if (animator != null && enter) {
                animator.setAnimationListener(object : Animation.AnimationListener {
                    override fun onAnimationRepeat(p0: Animation?) {}
                    override fun onAnimationStart(p0: Animation?) {}

                    override fun onAnimationEnd(p0: Animation?) {
                        (frameLayout.background as TransitionDrawable).startTransition(250)
                    }
                })
            }
        } else return AnimationUtils.loadAnimation(activity, R.anim.poster_exit_fragment_anim)

        return animator!!
    }

    companion object {
        private const val URL = "param1"

        fun newInstance(url: String): PosterFragment {
            val fragment = PosterFragment()
            val args = Bundle()
            args.putString(URL, url)
            fragment.arguments = args
            return fragment
        }
    }

}