package com.tiborfarago.omdbapp.details

import android.util.Log
import android.view.View
import com.tiborfarago.omdbapp.R
import com.tiborfarago.omdbapp.realm.Movie
import com.tiborfarago.omdbapp.retrofit.MovieDetails
import com.tiborfarago.omdbapp.retrofit.Service
import io.realm.Realm
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import javax.inject.Inject

class DetailsPresenter @Inject constructor(retrofit: Retrofit) : DetailsContract.Presenter {
    private var shouldAnimShow = false
    private var isFullPlotToShow = false

    private val service = retrofit.create(Service::class.java)

    private var realm = Realm.getDefaultInstance()
    private var imdbID: String? = null
    private var view: DetailsContract.View? = null

    private lateinit var movie: Movie

    override fun attachView(view: DetailsContract.View) {
        this.view = view
    }

    override fun getMovie(imdbID: String?) {
        this.imdbID = imdbID
        movie = realm.where(Movie::class.java).equalTo("imdbID", imdbID).findFirst()!!
        if (movie.isThereDetails()) showData()

        view?.loadCover(movie.poster)
        view?.setTitle(movie.title)
        view?.changeSaveButton(movie.isSaved)

        view?.setProgressBarVisibility(View.VISIBLE)
        service.movieDetails(movie.imdbID, "short").enqueue(object : Callback<MovieDetails> {
            override fun onFailure(call: Call<MovieDetails>?, t: Throwable?) {
                // todo handle errors
                showData()
            }

            override fun onResponse(call: Call<MovieDetails>?, response: Response<MovieDetails>?) {
                if (!realm.isClosed)
                    realm.executeTransactionAsync({ realm ->
                        run {
                            val body = response?.body()
                            val movie = realm.where(Movie::class.java).equalTo("imdbID", imdbID).findFirst()

                            body?.ratings?.forEach {
                                if (it.source == "Internet Movie Database") movie?.imdbRating = it.value
                                if (it.source == "Rotten Tomatoes") movie?.rottenTomatoesRating = it.value
                                if (it.source == "Metacritic") movie?.metacriticRating = it.value
                            }

                            movie?.shortPlot = body?.plot!!
                            movie?.rated = body.rated
                            movie?.runtime = body.runtime
                            movie?.genre = body.genre
                            movie?.director = body.director
                            movie?.writer = body.writer
                            movie?.actors = body.actors
                            movie?.language = body.language
                            movie?.awards = body.awards

                            // These are needed!
                            if (body.released != null && body.released != "N/A") movie?.setReleaseDate(body.released)
                            if (body.dVD != null && body.dVD != "N/A") movie?.setDvdDate(body.dVD)
                            if (body.boxOffice != null) movie?.boxOffice = body.boxOffice
                            if (body.production != null) movie?.production = body.production
                            if (body.website != null) movie?.website = body.website
                        }
                    }, {
                        showData()
                    }, {
                        Log.e("Realm ERROR", it.message)
                        showData()
                    })
            }

        })
    }

    override fun getFullPlot() {
        isFullPlotToShow = !isFullPlotToShow
        if (!isFullPlotToShow) {
            view?.setFABsrc(R.drawable.ic_keyboard_arrow_down_black_36px)
            view?.setPlot(movie.shortPlot)
            return
        }
        if (isFullPlotToShow && movie.longPlot != "" && movie.longPlot != "N/A") {
            view?.setPlot(movie.longPlot)
            view?.setFABsrc(R.drawable.ic_keyboard_arrow_up_black_36px)
        }

        view?.setProgressBarVisibility(View.VISIBLE)
        service.movieDetails(movie.imdbID, "full").enqueue(object : Callback<MovieDetails> {
            override fun onFailure(call: Call<MovieDetails>?, t: Throwable?) {
                // todo handle errors
            }

            override fun onResponse(call: Call<MovieDetails>?, response: Response<MovieDetails>?) {
                realm.executeTransactionAsync({realm -> run {
                    val movie = realm.where(Movie::class.java).equalTo("imdbID", this@DetailsPresenter.imdbID).findFirst()
                    response?.body()?.plot?.run {
                        movie?.longPlot = this
                    }
                } }, {
                    view?.setProgressBarVisibility(View.GONE)
                    view?.setFABsrc(R.drawable.ic_keyboard_arrow_up_black_36px)
                    view?.setPlot(movie.longPlot)
                }, {
                    view?.setProgressBarVisibility(View.GONE)
                    Log.e("Realm ERROR", it.message)
                })
            }
        })
    }

    fun showData() {
        view?.setPlot(movie.shortPlot)
        view?.setIMDBRating(movie.imdbRating)
        view?.setRottenRating(movie.rottenTomatoesRating)
        view?.setMetaRating(movie.metacriticRating)

        view?.setDetails(movie)
        view?.setProgressBarVisibility(View.GONE)

        showScrollViewIfAnimEnded()
    }

    override fun showScrollViewIfAnimEnded() {
        if (shouldAnimShow) view?.setScrollViewVisibility(View.VISIBLE)
        else shouldAnimShow = true
    }

    override fun onFullScreenClick() {
        view?.showPoster(movie.poster)
    }

    override fun detachView() {
        view = null
        realm.close()
    }

    override fun saveMovie() {
        realm.executeTransaction {
            movie.isSaved = !movie.isSaved
        }
        view?.changeSaveButton(movie.isSaved)
    }

    override fun isMovieSaved(): Boolean {
        if (!realm.isClosed) return movie.isSaved
        return false
    }
}