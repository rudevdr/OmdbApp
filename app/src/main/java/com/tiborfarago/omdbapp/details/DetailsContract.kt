package com.tiborfarago.omdbapp.details

import com.tiborfarago.omdbapp.realm.Movie

interface DetailsContract {
    interface View {
        fun loadCover(url: String)
        fun showPoster(url: String)
        fun setTitle(title: String)
        fun setIMDBRating(rating: String)
        fun setRottenRating(rating: String)
        fun setMetaRating(rating: String)
        fun setPlot(plot: String?)
        fun setScrollViewVisibility(visibility: Int)
        fun setDetails(movie: Movie)
        fun setProgressBarVisibility(visibility: Int)
        fun setFABsrc(drawable: Int)
        fun changeSaveButton(isSaved: Boolean)
    }
    interface Presenter {
        fun attachView(view: View)
        fun detachView()
        fun getMovie(imdbID: String?)
        fun getFullPlot()
        fun onFullScreenClick()
        fun showScrollViewIfAnimEnded()
        fun saveMovie()
        fun isMovieSaved(): Boolean
    }
}