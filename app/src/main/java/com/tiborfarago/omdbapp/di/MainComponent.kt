package com.tiborfarago.omdbapp.di

import com.tiborfarago.omdbapp.details.DetailsFragment
import com.tiborfarago.omdbapp.main.MainFragment
import dagger.Component

@CustomAnnotations.PerActivity
@Component(dependencies = [(ApplicationComponent::class)])
interface MainComponent {
    fun inject(mainFragment: MainFragment)
    fun inject(detailsFragment: DetailsFragment)
}