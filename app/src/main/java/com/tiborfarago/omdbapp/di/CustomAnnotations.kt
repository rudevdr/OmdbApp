package com.tiborfarago.omdbapp.di

import javax.inject.Scope

class CustomAnnotations {
    @Scope
    @Retention(AnnotationRetention.RUNTIME)
    annotation class PerActivity

}