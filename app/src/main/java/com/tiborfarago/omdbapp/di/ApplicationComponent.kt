package com.tiborfarago.omdbapp.di

import dagger.Component
import retrofit2.Retrofit
import javax.inject.Singleton

@Singleton
@Component(modules = [(RetrofitModule::class)])
interface ApplicationComponent {
    fun retrofit(): Retrofit
}