package com.tiborfarago.omdbapp

import android.support.v4.app.Fragment

fun Fragment.addFragment(fragment: Fragment) {
    this.activity?.supportFragmentManager
            ?.beginTransaction()
            ?.setCustomAnimations(
                    R.anim.details_enter_fragment_anim,
                    R.anim.details_exit_fragment_anim,
                    R.anim.details_pop_enter_fragment_anim,
                    R.anim.details_pop_exit_fragment_anim)
            ?.hide(this)
            ?.add(R.id.frame_layout, fragment)
            ?.addToBackStack(null)
            ?.commit()
}

fun Fragment.openImage(fragment: Fragment) {
    this.activity?.supportFragmentManager
            ?.beginTransaction()
            ?.setCustomAnimations(
                    R.anim.poster_enter_fragment_anim,
                    R.anim.poster_exit_fragment_anim
            )
            ?.add(R.id.frame_layout, fragment)
            ?.addToBackStack(null)
            ?.commit()
}