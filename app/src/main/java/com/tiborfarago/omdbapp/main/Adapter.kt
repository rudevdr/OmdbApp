package com.tiborfarago.omdbapp.main

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.tiborfarago.omdbapp.R
import com.tiborfarago.omdbapp.realm.Movie
import io.realm.OrderedRealmCollection
import io.realm.RealmRecyclerViewAdapter

class Adapter(
        data: OrderedRealmCollection<Movie>?,
        val context: Context,
        val clickHandler: (Movie?) -> Unit,
        val footerClickHandler: () -> Unit) : RealmRecyclerViewAdapter<Movie, RecyclerView.ViewHolder>(data, true) {
    private val TYPE_ITEM = 0
    private val TYPE_FOOTER = 1

    private var footerVisibility: Boolean = false

    override fun getItemViewType(position: Int): Int {
        return if (position == this.itemCount.minus(1)) TYPE_FOOTER else TYPE_ITEM
    }

    override fun getItemCount(): Int {
        return super.getItemCount() + 1
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): RecyclerView.ViewHolder? {
        return when (viewType) {
            TYPE_ITEM -> ViewHolder(LayoutInflater.from(parent?.context).inflate(R.layout.list_item_movie, parent, false))
            TYPE_FOOTER -> FooterViewHolder(LayoutInflater.from(parent?.context).inflate(R.layout.list_item_footer, parent, false))
            else -> null
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder?, position: Int) {
        when (holder) {
            is ViewHolder -> holder.bindItem(this.getItem(position))
            is FooterViewHolder -> holder.hide(footerVisibility)
        }
    }

    inner class ViewHolder(private val view: View) : RecyclerView.ViewHolder(view), View.OnClickListener {
        private var movie: Movie? = null
        private lateinit var image: ImageView

        init {
            view.setOnClickListener(this)
        }

        override fun onClick(view: View?) {
            clickHandler.invoke(movie)
        }

        fun bindItem(movie: Movie?) {
            this.movie = movie
            val title = view.findViewById<TextView>(R.id.list_item_title)
            val year = view.findViewById<TextView>(R.id.list_item_year)
            val cloudIcon = view.findViewById<ImageView>(R.id.list_item_saved_icon)
            image = view.findViewById(R.id.list_item_image_view)

            title.text = movie?.title
            year.text = movie?.year

            cloudIcon.visibility = if (movie?.isSaved!!) View.VISIBLE else View.GONE

            if (movie?.poster != "N/A")
                Glide.with(context)
                        .load(movie?.poster)
                        .thumbnail(0.1f)
                        .apply(RequestOptions()
                                .error(R.drawable.ic_error_outline_black_36px)
                                .fallback(R.drawable.ic_help_outline_black_36px)
                                .centerCrop())
                        .into(image)
            else image.setImageDrawable(null)
        }
    }

    inner class FooterViewHolder(private val view: View): RecyclerView.ViewHolder(view), View.OnClickListener {
        val height: Int
        init {
            view.setOnClickListener(this)
            height = this.view.layoutParams.height
        }

        override fun onClick(view: View?) {
            footerClickHandler.invoke()
        }

        fun hide(isShown: Boolean) {
            if (isShown) this.view.visibility = View.VISIBLE
            else this.view.visibility = View.GONE
        }
    }

    fun setFooterVisibility(isVisible: Boolean) {
        footerVisibility = isVisible
        this.notifyItemChanged(this.itemCount-1)
    }
}
