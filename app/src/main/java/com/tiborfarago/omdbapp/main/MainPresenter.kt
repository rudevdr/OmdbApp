package com.tiborfarago.omdbapp.main

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.Observer
import com.tiborfarago.omdbapp.realm.Movie
import com.tiborfarago.omdbapp.retrofit.MovieList
import com.tiborfarago.omdbapp.retrofit.Service
import io.realm.Case
import io.realm.Realm
import io.realm.RealmResults
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import java.util.*
import javax.inject.Inject

class MainPresenter @Inject constructor(val retrofit: Retrofit) : MainContract.Presenter {
    private var service = retrofit.create(Service::class.java)

    private lateinit var realmResults: RealmResults<Movie>
    private var realm = Realm.getDefaultInstance()
    private var view: MainContract.View? = null

    private var timer = Timer()

    private var title: String = ""
    private var pageNumber = 1

    private var liveData: MutableLiveData<String> = MutableLiveData()
    private lateinit var observer: Observer<String>

    override fun attachView(view: MainContract.View) {
        this.view = view

        observer = Observer {
            view.showLoading(true)

            networkTask(pageNumber)
        }

        liveData.observeForever(observer)
    }

    override fun detachView() {
        this.view = null

        liveData.removeObserver(observer)
        realm.close()
    }

    override fun searchMovies(title: String) {
        this.title = title
        pageNumber = 1

        timer.cancel()
        timer = Timer()

        if (title != "") {
            timer.schedule(object : TimerTask() {
                override fun run() {
                    liveData.postValue(title)
                }
            }, 1000)
            realmResults = realm.where(Movie::class.java).contains("title", title, Case.INSENSITIVE).findAll()
        } else {
            realmResults = realm.where(Movie::class.java).sort("title").findAll()
            view?.showFooter(false)
        }

        view?.changeDataset(realmResults)
    }

    override fun loadMore() {
        if (pageNumber != -1) {
            pageNumber += 1

            networkTask(pageNumber, true)
        } else view?.showFooter(false)
    }

    private fun networkTask(pageNumber: Int, shouldLoadMore: Boolean = false) {
        service.movieList(title, pageNumber).enqueue(object : Callback<MovieList> {
            override fun onFailure(call: Call<MovieList>?, t: Throwable?) {
                // todo error handling
                view?.showLoading(false)
            }

            override fun onResponse(call: Call<MovieList>?, response: Response<MovieList>?) {
                if (response?.body()?.response.equals("False")) {
                    if (pageNumber == 1) view?.showSnackbar("No such movies")
                    else this@MainPresenter.pageNumber = -1
                }

                val count = response?.body()?.movies?.size
                loadMoreIfAllExist(response?.body()?.movies, shouldLoadMore)
                view?.showLoading(false)

                when (count) {
                    10 -> view?.showFooter(true)
                    in 0..9, null -> {
                        this@MainPresenter.pageNumber = -1
                        view?.showFooter(false)
                    }
                }
            }
        })

    }

    override fun initData() {
        realmResults = realm.where(Movie::class.java).sort("title").findAll()
        view?.changeDataset(realmResults)
    }

    fun loadMoreIfAllExist(movies: List<com.tiborfarago.omdbapp.retrofit.Movie>?, shouldLoadMore: Boolean = false) {
        var counter = 0

        if (!realm.isClosed)
            realm.executeTransactionAsync {
                movies?.forEachIndexed { _, movie ->
                    if (it.where(Movie::class.java).equalTo("imdbID", movie.imdbID).count() != 0L)
                        counter++

                    it.copyToRealmOrUpdate(Movie().also {
                        it.title = movie.title
                        it.imdbID = movie.imdbID
                        it.poster = movie.poster
                        it.type = movie.type
                        it.year = movie.year
                    })
                }
                if (shouldLoadMore && counter == 10) loadMore()
            }
    }
}