package com.tiborfarago.omdbapp.main


import android.content.Context
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.text.Editable
import android.text.TextWatcher
import android.view.*
import android.view.inputmethod.InputMethodManager
import android.widget.*
import com.tiborfarago.omdbapp.*
import com.tiborfarago.omdbapp.details.DetailsFragment
import com.tiborfarago.omdbapp.realm.Movie
import io.realm.RealmResults
import kotlinx.android.synthetic.main.fragment_main.*
import kotlinx.android.synthetic.main.toolbar.*
import javax.inject.Inject

class MainFragment : Fragment(), MainContract.View, MainContract.OnBackPressed {
    private val SEARCH_VISIBILITY = "visibility"

    @Inject lateinit var mainPresenter: MainPresenter

    private var adapter: Adapter? = null
    private lateinit var linearLayoutManager: LinearLayoutManager

    private lateinit var inputMethodManager: InputMethodManager

    override fun backPressed() {
        if (search_main_toolbar_layout.visibility == View.VISIBLE)
            closeAll()
        else
            activity?.onBackPressed()
    }

    override fun onStart() {
        super.onStart()

        setHasOptionsMenu(true)
        AddToolbar.addToolbar(activity, view, R.id.toolbar_main_fragment, false)
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)

        App.INSTANCE.mainComponent.inject(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_main, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if (savedInstanceState != null)
            search_main_toolbar_layout.visibility = savedInstanceState.getInt(SEARCH_VISIBILITY, View.GONE)

        mainPresenter.attachView(this)

        inputMethodManager = activity?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager

        linearLayoutManager = LinearLayoutManager(context, LinearLayout.VERTICAL, false)
        recycler_view_main_fragment.layoutManager = linearLayoutManager

        cancel_search_button.setOnClickListener {
            closeAll()
        }

        search_text.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {}
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

            override fun onTextChanged(text: CharSequence?, p1: Int, p2: Int, p3: Int) {
                mainPresenter.searchMovies(text.toString().trim())
            }
        })

        mainPresenter.initData()
    }

    private fun closeAll() {
        search_main_toolbar_layout.visibility = View.GONE
        search_text.clearFocus()
        search_text.setText("")
        inputMethodManager.hideSoftInputFromWindow(search_text.windowToken, 0)
    }

    override fun changeDataset(data: RealmResults<Movie>?) {
        if (adapter == null) {
            adapter = Adapter(data, context!!, { movie ->
                addFragment(DetailsFragment.newInstance(movie?.imdbID))
            }, {
                mainPresenter.loadMore()
            })
            recycler_view_main_fragment.setHasFixedSize(true)
            recycler_view_main_fragment.adapter = adapter
        } else {
            adapter?.updateData(data)
        }
    }

    override fun showSnackbar(message: String) {
        Snackbar.make(fragment_main_linear_layout, message, Snackbar.LENGTH_SHORT).show()
    }

    override fun showLoading(isLoading: Boolean) {
        toolbar_progress_bar.visibility = if (isLoading) View.VISIBLE else View.GONE
    }

    override fun showFooter(isShown: Boolean) {
        adapter?.setFooterVisibility(isShown)
    }

    companion object {
        fun newInstance(): MainFragment {
            return MainFragment()
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()

        mainPresenter.detachView()
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater?.inflate(R.menu.menu_main, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when(item?.itemId) {
            R.id.menu_item_search -> {
                if (search_main_toolbar_layout.visibility == View.GONE) search_main_toolbar_layout.visibility = View.VISIBLE
                search_text.requestFocus()
                inputMethodManager.showSoftInput(search_text, InputMethodManager.SHOW_IMPLICIT)
            }
            R.id.menu_item_about -> Toast.makeText(context, "about", Toast.LENGTH_SHORT).show()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.putInt(SEARCH_VISIBILITY, search_main_toolbar_layout.visibility)

        super.onSaveInstanceState(outState)
    }
}
