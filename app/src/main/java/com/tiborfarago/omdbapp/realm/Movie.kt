package com.tiborfarago.omdbapp.realm

import io.realm.RealmModel
import io.realm.annotations.Index
import io.realm.annotations.PrimaryKey
import io.realm.annotations.RealmClass
import java.text.SimpleDateFormat
import java.util.*

@RealmClass
open class Movie : RealmModel {
    @Index var title: String = ""
    var year: String = ""
    @Index @PrimaryKey var imdbID: String = ""
    var type: String = ""
    var poster: String =""
    var rated: String = ""
    var released: Date = Date(Long.MIN_VALUE)
        private set
    var runtime: String = ""
    var genre: String = ""
    var director: String = ""
    var writer: String = ""
    var actors: String = ""
    var language: String = ""
    var awards: String = ""
    var dvd: Date = Date(Long.MIN_VALUE)
        private set
    var boxOffice: String = ""
    var production: String = ""
    var website: String = ""
    var imdbRating: String = ""
    var rottenTomatoesRating: String = ""
    var metacriticRating: String = ""
    var shortPlot: String = ""
    var longPlot: String = ""
    var isSaved: Boolean = false

    fun setReleaseDate(dateString: String) {
        released = stringToDate(dateString)
    }

    fun setDvdDate(dateString: String) {
        dvd = stringToDate(dateString)
    }

    fun getReleaseDate() = dateToString(released)
    fun getDvdDate() = dateToString(dvd)

    private fun dateToString(date: Date): String {
        return if (date == Date(Long.MIN_VALUE)) ""
        else SimpleDateFormat("yyyy. MMM. dd.", Locale.getDefault()).format(date)
    }

    private fun stringToDate(dateString: String): Date =
        SimpleDateFormat("dd MMM yyyy", Locale.getDefault()).parse(dateString)

    fun isThereDetails() =
            runtime != "" ||
            director != "" ||
            writer != "" ||
            actors != "" ||
            language != "" ||
            awards != ""
}