package com.tiborfarago.omdbapp

import android.support.v4.app.FragmentActivity
import android.support.v7.widget.Toolbar
import android.view.View

class AddToolbar {
    companion object {
        fun addToolbar(
                activity: FragmentActivity?,
                view: View?,
                id: Int,
                hasUpButton: Boolean,
                title: String? = activity?.resources?.getText(activity.resources.getIdentifier("app_name", "string", activity.packageName)).toString()
        ) {
            val toolbar = view?.findViewById<Toolbar>(id)
            (activity as MainActivity).setSupportActionBar(toolbar)
            activity.supportActionBar?.setDisplayHomeAsUpEnabled(hasUpButton)
            activity.supportActionBar?.title = title
        }
    }
}