package com.tiborfarago.omdbapp.retrofit

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class MovieList(
        @SerializedName("Search")
        @Expose
        val movies: List<Movie>,
        @SerializedName("totalResults")
        @Expose
        val totalResults: String,
        @SerializedName("Response")
        @Expose
        val response: String
)